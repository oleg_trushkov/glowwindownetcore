﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Shapes;

namespace GlowEffect
{
    enum Location
    {
        Left, Top, Right, Bottom
    }

    [TemplatePart(Name = "PART_Glow", Type = typeof(Border))]
    [TemplatePart(Name = "PART_Border", Type = typeof(Path))]
    class GlowWindow : Window
    {
        public int glowThickness = 10, cornerArea = 10;
        public Location Location;
        public Action Update;
        Border Glow;
        Path Border;

        static GlowWindow()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(GlowWindow), new FrameworkPropertyMetadata(typeof(GlowWindow)));
        }

        public GlowWindow()
        {
            WindowStyle = WindowStyle.None;
            ResizeMode = ResizeMode.NoResize;
            AllowsTransparency = true;
            SnapsToDevicePixels = true;
            ShowInTaskbar = false;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Glow = (Border)GetTemplateChild("PART_Glow");
            if (Glow == null)
                throw new Exception("PART_Glow not found.");

            Border = (Path)GetTemplateChild("PART_Border");
            if (Border == null)
                throw new Exception("PART_Border not found.");

            switch (Location)
            {
                case Location.Left:
                    Border.Data = Geometry.Parse("M 0,0 0,1");
                    Border.Margin = new Thickness(glowThickness - 1, glowThickness - 1, 0, glowThickness - 2);
                    break;
                case Location.Top:
                    Border.Data = Geometry.Parse("M 0,0 1,0");
                    Border.Margin = new Thickness(-1, glowThickness - 1, -1, 0);
                    break;
                case Location.Right:
                    Border.Data = Geometry.Parse("M 0,0 0,1");
                    Border.Margin = new Thickness(0, glowThickness - 1, glowThickness - 1, glowThickness - 2);
                    break;
                case Location.Bottom:
                    Border.Data = Geometry.Parse("M 0,0 1,0");
                    Border.Margin = new Thickness(-1, 0, -1, glowThickness - 1);
                    break;
            }

            Update();
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            IntPtr hwnd = new WindowInteropHelper(this).Handle;
            uint ws_ex = User32.GetWindowLong(hwnd, (int)GetWindowLongFlags.GWL_EXSTYLE);
            ws_ex |= (uint)WindowExStyles.WS_EX_TOOLWINDOW;
            uint ws = User32.GetWindowLong(hwnd, (int)GetWindowLongFlags.GWL_STYLE);
            ws &= ~(uint)WindowStyles.WS_SYSMENU;
            ws |= (uint)WindowStyles.WS_POPUP;
            User32.SetWindowLong(hwnd, (int)GetWindowLongFlags.GWL_EXSTYLE, ws_ex);
            User32.SetWindowLong(hwnd, (int)GetWindowLongFlags.GWL_STYLE, ws);
            HwndSource.FromHwnd(hwnd).AddHook(WndProc);
        }

        public virtual IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch ((WindowsMessages)msg)
            {
                //case WindowsMessages.WM_MOUSEMOVE:
                //    RECT rect = new RECT();
                //    User32.GetWindowRect(hwnd, ref rect);
                //    const SetWindowPosFlags flags = (SetWindowPosFlags.SWP_NOMOVE | SetWindowPosFlags.SWP_NOACTIVATE);
                //    User32.SetWindowPos(hwnd, new IntPtr(1), rect.left, rect.right, rect.right - rect.left, rect.bottom - rect.top, flags);
                //    break;
                case WindowsMessages.WM_SETCURSOR:
                    handled = true;
                    SetCursor();
                    return new IntPtr(1);
                case WindowsMessages.WM_LBUTTONDOWN:
                    Owner.Activate();
                    var result = GetResizeMode();
                    IntPtr ownerHwnd = new WindowInteropHelper(Owner).Handle;
                    User32.SendNotifyMessage(ownerHwnd, (int)WindowsMessages.WM_NCLBUTTONDOWN, (IntPtr)result, IntPtr.Zero);
                    break;
            }
            return IntPtr.Zero;
        }

        public virtual IntPtr OwnerWndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch ((WindowsMessages)msg)
            {
                case WindowsMessages.WM_WINDOWPOSCHANGED:
                    posChanged = true;
                    break;
                case WindowsMessages.WM_MOUSEMOVE:
                    IntPtr handle = User32.LoadCursor(IntPtr.Zero, (int)IdcStandardCursors.IDC_ARROW);
                    User32.SetCursor(handle);
                    break;
            }
            return IntPtr.Zero;
        }

        private void SetCursor()
        {
            HitTest mode = GetResizeMode();
            IntPtr handle = User32.LoadCursor(IntPtr.Zero, (int)IdcStandardCursors.IDC_ARROW);

            switch (mode)
            {
                case HitTest.HTTOP:
                case HitTest.HTBOTTOM:
                    handle = User32.LoadCursor(IntPtr.Zero, (int)IdcStandardCursors.IDC_SIZENS);
                    break;
                case HitTest.HTLEFT:
                case HitTest.HTRIGHT:
                    handle = User32.LoadCursor(IntPtr.Zero, (int)IdcStandardCursors.IDC_SIZEWE);
                    break;
                case HitTest.HTTOPLEFT:
                case HitTest.HTBOTTOMRIGHT:
                    handle = User32.LoadCursor(IntPtr.Zero, (int)IdcStandardCursors.IDC_SIZENWSE);
                    break;
                case HitTest.HTTOPRIGHT:
                case HitTest.HTBOTTOMLEFT:
                    handle = User32.LoadCursor(IntPtr.Zero, (int)IdcStandardCursors.IDC_SIZENESW);
                    break;
            }

            User32.SetCursor(handle);
        }

        private POINT GetRelativeMousePosition()
        {
            var handle = new WindowInteropHelper(this).Handle;
            POINT point = new POINT();
            User32.GetCursorPos(ref point);
            User32.ScreenToClient(handle, ref point);
            return point;
        }

        private HitTest GetResizeMode()
        {
            var handle = new WindowInteropHelper(this).Handle;
            HitTest mode = HitTest.HTNOWHERE;
            RECT rect = new RECT();
            POINT point = GetRelativeMousePosition();
            User32.GetWindowRect(handle, ref rect);

            switch (Location)
            {
                case Location.Top:
                    int width = rect.right - rect.left;
                    if (point.x < cornerArea) mode = HitTest.HTTOPLEFT;
                    else if (point.x > width - cornerArea) mode = HitTest.HTTOPRIGHT;
                    else mode = HitTest.HTTOP;
                    break;
                case Location.Bottom:
                    width = rect.right - rect.left;
                    if (point.x < cornerArea) mode = HitTest.HTBOTTOMLEFT;
                    else if (point.x > width - cornerArea) mode = HitTest.HTBOTTOMRIGHT;
                    else mode = HitTest.HTBOTTOM;
                    break;
                case Location.Left:
                    int height = rect.bottom - rect.top;
                    if (point.y < 2 * cornerArea) mode = HitTest.HTTOPLEFT;
                    else if (point.y > height - 2 * cornerArea) mode = HitTest.HTBOTTOMLEFT;
                    else mode = HitTest.HTLEFT;
                    break;
                case Location.Right:
                    height = rect.bottom - rect.top;
                    if (point.y < 2 * cornerArea) mode = HitTest.HTTOPRIGHT;
                    else if (point.y > height - 2 * cornerArea) mode = HitTest.HTBOTTOMRIGHT;
                    else mode = HitTest.HTRIGHT;
                    break;
            }

            return mode;
        }

        public void OwnerChanged()
        {
            IntPtr ownerHwnd = new WindowInteropHelper(Owner).Handle;
            uint ws = User32.GetWindowLong(ownerHwnd, (int)GetWindowLongFlags.GWL_STYLE);
            ws |= (uint)WindowStyles.WS_CAPTION;
            User32.SetWindowLong(ownerHwnd, (int)GetWindowLongFlags.GWL_STYLE, ws);
            HwndSource.FromHwnd(ownerHwnd).AddHook(OwnerWndProc);

            switch (Location)
            {
                case Location.Left:
                    Update = delegate
                    {
                        if (Glow != null)
                            Glow.Margin = new Thickness(glowThickness, glowThickness, -glowThickness, glowThickness);

                        Left = Owner.Left - glowThickness;
                        Top = Owner.Top - glowThickness;
                        Width = glowThickness;
                        Height = Owner.Height + glowThickness * 2;
                    };
                    break;

                case Location.Top:
                    Update = delegate
                    {
                        if (Glow != null)
                            Glow.Margin = new Thickness(0, glowThickness, 0, -glowThickness);

                        Left = Owner.Left;
                        Top = Owner.Top - glowThickness;
                        Width = Owner.Width;
                        Height = glowThickness;
                    };
                    break;

                case Location.Right:
                    Update = delegate
                    {
                        if (Glow != null)
                            Glow.Margin = new Thickness(-glowThickness, glowThickness, glowThickness, glowThickness);

                        Left = Owner.Left + Owner.Width;
                        Top = Owner.Top - glowThickness;
                        Width = glowThickness;
                        Height = Owner.Height + glowThickness * 2;
                    };
                    break;

                case Location.Bottom:
                    Update = delegate
                    {
                        if (Glow != null)
                            Glow.Margin = new Thickness(0, -glowThickness, 0, glowThickness);

                        Left = Owner.Left;
                        Top = Owner.Top + Owner.Height;
                        Width = Owner.Width;
                        Height = glowThickness;
                    };
                    break;
            }

            Owner.LocationChanged += delegate
            {
                Update();
            };

            Owner.SizeChanged += delegate
            {
                Update();
            };

            Owner.StateChanged += delegate
            {
                switch (Owner.WindowState)
                {
                    case WindowState.Maximized:
                        Hide();
                        break;
                    case WindowState.Minimized:
                        Hide();
                        break;
                    default:
                        Task.Run(async () =>
                        {
                            while (posChanged)
                            {
                                posChanged = false;
                                await Task.Delay(50);
                            }

                            Dispatcher.Invoke(() =>
                            {
                                Show();
                                Owner.Activate();
                            });
                        });
                        break;
                }
            };

            Owner.Activated += delegate
            {
                Binding activeBrushBinding = new Binding
                {
                    Path = new PropertyPath(GlowManager.ActiveGlowBrushProperty),
                    Source = Owner
                };
                SetBinding(ForegroundProperty, activeBrushBinding);
            };

            Owner.Deactivated += delegate
            {
                Binding activeBrushBinding = new Binding
                {
                    Path = new PropertyPath(GlowManager.InactiveGlowBrushProperty),
                    Source = Owner
                };
                SetBinding(ForegroundProperty, activeBrushBinding);
            };

            Update();
            Show();
        }

        private bool posChanged = false;
    }
}
